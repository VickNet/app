import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HttpModule } from '@angular/http';

/* ngx-translate */
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

/* website components */
import { HomeComponent } from './components/website/home';
import { NavbarComponent } from './components/website/navbar';
import { PageNotFoundComponent } from './components/website/page-not-found';

/* modules */
import { GuestModule } from './components/guest/guest.module';

/* guards */
import { AuthGuard } from './guards/auth.guard';
import { GuestGuard } from './guards/guest.guard';


const appRoutes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: '404',
    component: PageNotFoundComponent
  },
  {
    path: '**',
    redirectTo: '/404'
  }
];


@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    PageNotFoundComponent
  ],
  imports: [
    HttpModule,
    GuestModule,
    BrowserModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthGuard, GuestGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
