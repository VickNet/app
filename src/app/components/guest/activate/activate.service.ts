import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AuthService } from '../auth.service';

@Injectable()
export class ActivateService extends AuthService {

    constructor(
        private _http: Http
    ) {
        super(_http);
    }

    public activate(token) {
        return this.http.put(`${this.URL}email/activate`, token, this.options);
    }
}