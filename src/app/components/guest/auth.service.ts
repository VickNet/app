import { Injectable, Inject } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

import { environment } from '../../../environments/environment';

import { Observable } from 'rxjs/Observable';

import { tokenNotExpired } from 'angular2-jwt';

@Injectable()
export class AuthService {

  URL: string = environment.apiUrl;

  constructor(
    protected http: Http,
  ) {}

  loggedIn() {
    return tokenNotExpired();
  }

  protected options () {
    return new RequestOptions({
      headers: new Headers({
        'Content-Type': 'application/json'
      })
    });
  }

}