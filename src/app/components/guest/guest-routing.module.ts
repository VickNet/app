import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';

import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { ActivateComponent } from './activate/activate.component';
import { ResetComponent } from './reset/reset.component';
import { ResetPasswordComponent } from './reset-password';
import { ResetUsernameComponent } from './reset-username';

import { AuthGuard } from '../../guards/auth.guard';
import { GuestGuard } from '../../guards/guest.guard';

const profilesRoutes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'activate/:token',
    component: ActivateComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'reset/:token',
    component: ResetPasswordComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'recover/username',
    component: ResetUsernameComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'register',
    component: RegisterComponent,
    canActivate: [GuestGuard]
  },
  {
    path: 'reset',
    component: ResetComponent,
    canActivate: [GuestGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(profilesRoutes)],
  exports: [RouterModule]
})
export class GuestRoutingModule {}
