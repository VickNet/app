import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { LoadingComponent } from '../website/loading';
import { LoginComponent } from './login';
import { ResetComponent } from './reset';
import { RegisterComponent } from './register';
import { ActivateComponent } from './activate';
import { ResetPasswordComponent } from './reset-password';
import { ResetUsernameComponent } from './reset-username';

import { AuthService } from './auth.service';
import { RegisterService } from './register/register.service';
import { ActivateService } from './activate/activate.service';
import { LoginService } from './login/login.service';
import { ResetService } from './reset/reset.service';
import { ResetPasswordService } from './reset-password/reset-password.service';
import { ResetUsernameService } from './reset-username/reset-username.service';

import { GuestRoutingModule } from './guest-routing.module';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    FormsModule,
    FormsModule,
    CommonModule,
    GuestRoutingModule,
    ReactiveFormsModule,
    TranslateModule
  ],
  exports: [
    LoadingComponent
  ],
  declarations: [
    LoginComponent,
    ResetComponent,
    LoadingComponent,
    RegisterComponent,
    ActivateComponent,
    ResetUsernameComponent,
    ResetPasswordComponent
  ],
  providers: [
    AuthService,
    ActivateService,
    LoginService,
    RegisterService,
    ResetService,
    ResetPasswordService,
    ResetUsernameService
  ]
})
export class GuestModule {}
