import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { Router, ActivatedRoute, Params } from '@angular/router';
import { ResetPasswordService } from './reset-password.service';
import { tokenNotExpired } from 'angular2-jwt';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.scss']
})
export class ResetPasswordComponent implements OnInit {
  loading;
  success;
  error;
  form;
  valid: boolean;
  token: string;

  constructor(
    private _route: Router,
    private _fb: FormBuilder,
    private _resetPasswordService: ResetPasswordService,
    private _activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.form = this._fb.group({
      password: [
        '', [
          Validators.required,
          Validators.minLength(4)
        ]],
      token: [this.token]
    });
    this._activatedRoute.params.subscribe((params: Params) => {
      this.token = params['token'];
      this.form.controls['token'].setValue(this.token);
      this.valid = tokenNotExpired('', this.token);
    });
  }

  onSubmit() {
    this.loading = true;
    this.success = false;
    this.error = false;
    this._resetPasswordService.setNewPassword(this.form.value)
      .subscribe(
        data => {
          this.loading = false;
          this.success = true;
        },
        error => {
          this.loading = false;
          this.error = true;
        });
  }

}
