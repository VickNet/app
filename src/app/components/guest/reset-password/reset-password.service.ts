import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AuthService } from '../auth.service';

@Injectable()
export class ResetPasswordService extends AuthService {

    constructor(
        private _http: Http
    ) {
        super(_http);
    }

    setNewPassword(data) {
        return this._http.put(`${this.URL}users/savepassword`, data, this.options);
    }
}
