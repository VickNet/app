import { Injectable } from '@angular/core';
import { Http } from '@angular/http';



import { AuthService } from '../auth.service';

@Injectable()
export class ResetUsernameService extends AuthService {

    constructor(
        private _http: Http
    ) {
        super(_http);
    }

    resetUsername(email) {
        return this._http.get(`${this.URL}users/resetusername/${email}`, this.options);
    }
}
