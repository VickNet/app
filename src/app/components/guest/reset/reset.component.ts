import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { ResetService } from './reset.service';
@Component({
  selector: 'app-reset',
  templateUrl: './reset.component.html',
  styleUrls: ['./reset.component.scss']
})
export class ResetComponent implements OnInit {
  status;
  loading: boolean;
  error: boolean;
  success: boolean;
  public form: FormGroup;

    constructor(
      private _fb: FormBuilder,
      private _resetService: ResetService
    ) {}

     public ngOnInit() {
      this.form = this._fb.group({
         username: ''
       });
     }

     onSubmit() {
      this.loading = true;
      this.success = false;
      this.error = false;
       this._resetService.reset(this.form.value)
        .subscribe(
          data => {
            this.loading = false;
            this.success = true;
          },
          error => {
            this.status = error['_body'];
            this.loading = false;
            this.error = true;
          }
        );
     }

}
