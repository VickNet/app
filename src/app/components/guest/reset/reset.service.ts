import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

import { AuthService } from '../auth.service';

@Injectable()
export class ResetService extends AuthService {

    constructor(
        private _http: Http
    ) {
        super(_http);
    }

    reset(email) {
        return this._http.put(`${this.URL}users/resetpassword`, email, this.options)
    }
}