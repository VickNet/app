import { TranslateService } from '@ngx-translate/core';
import { Component, OnInit } from '@angular/core';

import { AuthService } from '../../guest/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor(
    public auth: AuthService,
    private _router: Router,
    private _translate: TranslateService
  ) { }

  ngOnInit() {
    const lang = localStorage.getItem('lang');

    if (!lang) {
      this._translate.setDefaultLang('en');
    } else {
      this._translate.setDefaultLang(lang);
    }
  }

  switchLanguage(language: string) {
    localStorage.setItem('lang', language);
    this._translate.use(language);
  }

  protected logout() {
    localStorage.clear();
    this._router.navigate(['login']);
  }

}
